from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth.views import LogoutView
from django.shortcuts import render, redirect
from django.http import HttpRequest
from django.urls import reverse_lazy, reverse
from django.views.generic import TemplateView, CreateView, ListView, DetailView, DeleteView, UpdateView
from .models import Profile


class UserDetailView(DetailView):
    template_name = 'authapp/user_info.html'
    context_object_name = 'user'
    queryset = User.objects.filter(is_active=1)


class UserCreateView(CreateView):
    form_class = UserCreationForm
    template_name = 'authapp/register.html'
    success_url = reverse_lazy('authapp:main_menu')

    def form_valid(self, form):
        response = super().form_valid(form)
        Profile.objects.create(user=self.object)
        username = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password1')

        user = authenticate(
            self.request,
            username=username,
            password=password
        )
        login(request=self.request, user=user)
        return response


class UserLogoutView(LogoutView):
    next_page = reverse_lazy('authapp:login')


def logout_view(request: HttpRequest):
    logout(request)
    return redirect(reverse('authapp:login'))


class MainMenuView(TemplateView):
    template_name = 'authapp/main_menu.html'


class UserUpdateView(UpdateView):
    model = Profile
    fields = 'age', 'bio', 'email'
    template_name_suffix = '_update_form'

    def get_success_url(self):
        return reverse(
            'authapp:user_info',
            kwargs={'pk': self.object.pk}
        )