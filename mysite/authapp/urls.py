from django.urls import path
from django.contrib.auth.views import LoginView
from .views import UserDetailView, UserCreateView, LogoutView, MainMenuView, UserUpdateView, logout_view

app_name = 'authapp'

urlpatterns = [
    path('', MainMenuView.as_view(), name='main_menu'),
    path('login/',
         LoginView.as_view(template_name='authapp/login.html',
                           redirect_authenticated_user=True),
         name='login'),
    path('register/', UserCreateView.as_view(), name='register'),
    path('logout/', logout_view, name='logout'),

    path('info/<int:pk>/', UserDetailView.as_view(), name='user_info'),
    path('info/update/<int:pk>/', UserUpdateView.as_view(), name='user_update')
]
