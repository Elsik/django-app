from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.urls import reverse_lazy, reverse
from django.views import View
from django.http import HttpRequest
from django.views.generic import ListView, CreateView, DetailView, UpdateView
from .models import Product, Order
from .forms import ProductForm


class ShopIndexView(View):
    def get(self, request: HttpRequest):
        return render(request, 'shopapp/index.html')


class ProductListView(ListView):
    template_name = 'shopapp/products_list.html'
    context_object_name = 'products'
    queryset = Product.objects.all()


class ProductCreateView(CreateView):
    model = Product
    fields = 'name', 'price', 'description', 'discount'
    success_url = reverse_lazy('shopapp:product_list')


class ProductDetailView(DetailView):
    template_name = 'shopapp/product_detail.html'
    context_object_name = 'product'
    queryset = Product.objects.all()


class ProductUpdateView(UpdateView):
    model = Product
    template_name_suffix = '_update_form'
    form_class = ProductForm

    def get_success_url(self):
        return reverse(
            'shopapp:product_detail',
            kwargs={'pk': self.object.pk}
        )


class OrderListView(LoginRequiredMixin, ListView):
    queryset = (
        Order.objects.select_related('user').prefetch_related('products').all()
    )


class OrderCreateView(CreateView):
    model = Order
    fields = 'user', 'delivery_address', 'products'
    success_url = reverse_lazy('shopapp:order_list')


class OrderDetailView(DetailView):
    queryset = (
        Order.objects.select_related('user').prefetch_related('products').all()
    )


class OrderUpdateView(UpdateView):
    model = Order
    fields = 'delivery_address', 'products'
    template_name_suffix = '_update_form'

    def get_success_url(self):
        return reverse(
            'shopapp:order_detail',
            kwargs={'pk': self.object.pk}
        )