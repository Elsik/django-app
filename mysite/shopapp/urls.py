from django.urls import path
from .views import (ShopIndexView,
                    ProductListView,
                    ProductCreateView,
                    ProductDetailView,
                    ProductUpdateView,
                    OrderCreateView,
                    OrderListView,
                    OrderDetailView,
                    OrderUpdateView)

app_name = 'shopapp'

urlpatterns = [
    path('', ShopIndexView.as_view(), name='index'),

    path('products/', ProductListView.as_view(), name='product_list'),
    path('products/create/', ProductCreateView.as_view(), name='product_create'),
    path('products/detail/<int:pk>/', ProductDetailView.as_view(), name='product_detail'),
    path('products/update/<int:pk>/', ProductUpdateView.as_view(), name='product_update'),

    path('orders/', OrderListView.as_view(), name='order_list'),
    path('orders/create/', OrderCreateView.as_view(), name='order_create'),
    path('orders/detail/<int:pk>/', OrderDetailView.as_view(), name='order_detail'),
    path('orders/update/<int:pk>/', OrderUpdateView.as_view(), name='order_update')
]